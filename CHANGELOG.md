Semantic Release Sample

## [1.5.5](https://bitbucket.org/ridermansousa/semantic-release-sample/compare/v1.5.4...v1.5.5) (2019-09-12)


### Bug Fixes

* **release:** improve release ([4dc01e1](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/4dc01e1))

## [1.5.4](https://bitbucket.org/ridermansousa/semantic-release-sample/compare/v1.5.3...v1.5.4) (2019-09-12)


### Bug Fixes

* **release:** leave default commit for release ([5f072f8](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/5f072f8))

## [1.5.3](https://bitbucket.org/ridermansousa/semantic-release-sample/compare/v1.5.2...v1.5.3) (2019-09-12)


### Bug Fixes

* **release:** move deployment to tag ([9ca95e8](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/9ca95e8))

## [1.5.2](https://bitbucket.org/ridermansousa/semantic-release-sample/compare/v1.5.1...v1.5.2) (2019-09-12)


### Bug Fixes

* **release:** do not publish npm, leave this for bitbucket pipeline ([92aa883](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/92aa883))

## [1.5.1](https://bitbucket.org/ridermansousa/semantic-release-sample/compare/v1.5.0...v1.5.1) (2019-09-12)


### Bug Fixes

* **release:** fix order to update package ([d546653](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/d546653))

# [1.5.0](https://bitbucket.org/ridermansousa/semantic-release-sample/compare/v1.4.2...v1.5.0) (2019-09-12)


### Features

* **bitbucket:** try to use pipes ([48072eb](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/48072eb))

## [1.4.2](https://bitbucket.org/ridermansousa/semantic-release-sample/compare/v1.4.1...v1.4.2) (2019-09-12)


### Bug Fixes

* **bitbucket:** fix bitbucket echo ([9e539d0](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/9e539d0))

## [1.4.1](https://bitbucket.org/ridermansousa/semantic-release-sample/compare/v1.4.0...v1.4.1) (2019-09-12)


### Bug Fixes

* **docs:** ficx readme documentation ([543d150](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/543d150))

# [1.4.0](https://bitbucket.org/ridermansousa/semantic-release-sample/compare/v1.3.1...v1.4.0) (2019-09-12)


### Features

* **npm:** create shareable configuration ([66c80f7](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/66c80f7))

## [1.3.1](https://bitbucket.org/ridermansousa/semantic-release-sample/compare/v1.3.0...v1.3.1) (2019-09-12)


### Bug Fixes

* **readme:** add more information about the project on readme ([bb9ef31](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/bb9ef31)), closes [#1](https://bitbucket.org/ridermansousa/semantic-release-sample/issue/1)

# [1.3.0](https://bitbucket.org/ridermansousa/semantic-release-sample/compare/v1.2.1...v1.3.0) (2019-09-12)


### Features

* **release:** add slack notification ([0a1f27f](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/0a1f27f))

# [1.2.0](https://bitbucket.org/ridermansousa/semantic-release-sample/compare/v1.1.12...v1.2.0) (2019-09-11)


### Features

* **release:** do not skip ci ([e944337](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/e944337))

## [1.1.12](https://bitbucket.org/ridermansousa/semantic-release-sample/compare/v1.1.11...v1.1.12) (2019-09-11)


### Bug Fixes

* **release:** fix tag format ([44f188c](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/44f188c))
* **release:** remove exec plugin ([fb7724d](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/fb7724d))
* **release:** run over any tag ([f96da33](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/f96da33))
* **release:** version fix ([228ad22](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/228ad22))

## [1.0.1](https://bitbucket.org/ridermansousa/semantic-release-sample/compare/v-1.0.0...v-1.0.1) (2019-09-11)


### Bug Fixes

* **release:** remove exec plugin ([fb7724d](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/fb7724d))

# 1.0.0 (2019-09-11)


### Bug Fixes

* **bitbucket:** add install on step to deploy ([5734393](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/5734393))
* **doc:** fix doc on readme ([f13f475](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/f13f475))
* **package:** fix commit hook ([7f7a28e](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/7f7a28e))
* **readme:** add more details ([e713d32](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/e713d32))
* **release:** add debug to release ([d94fafb](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/d94fafb))
* **release:** add merge to master ([7899cfd](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/7899cfd))
* **release:** add missing package ([a54cdbd](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/a54cdbd))
* **release:** bing back release notes generator ([53831ec](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/53831ec))
* **release:** fix exec cmd ([0e6a5a2](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/0e6a5a2))
* **release:** fix release exec ([c425e01](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/c425e01))
* **release:** fix tag format ([44f188c](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/44f188c))
* **release:** made push ([60d4171](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/60d4171))
* **release:** remove debug config ([106cf4c](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/106cf4c))
* **release:** remove release generator plugin ([ef2e416](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/ef2e416))
* **release:** update release cofig leave defaults ([da262de](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/da262de))
* **release:** use branch production on release ([a67a332](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/a67a332))
* **release:** version fix ([228ad22](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/228ad22))
* **version:** add npm plugin options ([7f523d5](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/7f523d5))


### Features

* **first:** first commit ([2c189af](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/2c189af))
* **package:** add npm lib to the semantic release ([4b5da5e](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/4b5da5e))
* **second:** another commit, second ([0c90834](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/0c90834))

## [1.1.11](https://bitbucket.org/ridermansousa/semantic-release-sample/compare/v1.1.10...v1.1.11) (2019-09-11)


### Bug Fixes

* **release:** fix exec cmd ([0e6a5a2](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/0e6a5a2))

## [1.1.10](https://bitbucket.org/ridermansousa/semantic-release-sample/compare/v1.1.9...v1.1.10) (2019-09-11)


### Bug Fixes

* **release:** made push ([60d4171](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/60d4171))

## [1.1.9](https://bitbucket.org/ridermansousa/semantic-release-sample/compare/v1.1.8...v1.1.9) (2019-09-11)


### Bug Fixes

* **release:** fix release exec ([c425e01](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/c425e01))

## [1.1.8](https://bitbucket.org/ridermansousa/semantic-release-sample/compare/v1.1.7...v1.1.8) (2019-09-11)


### Bug Fixes

* **release:** add merge to master ([7899cfd](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/7899cfd))
* **release:** add missing package ([a54cdbd](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/a54cdbd))

## [1.1.7](https://bitbucket.org/ridermansousa/semantic-release-sample/compare/v1.1.6...v1.1.7) (2019-09-11)


### Bug Fixes

* **doc:** fix doc on readme ([f13f475](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/f13f475))
* **readme:** add more details ([e713d32](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/e713d32))
* **release:** use branch production on release ([a67a332](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/a67a332))

## [1.1.6](https://bitbucket.org/ridermansousa/semantic-release-sample/compare/v1.1.5...v1.1.6) (2019-09-11)


### Bug Fixes

* **release:** update release cofig leave defaults ([da262de](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/da262de))

## [1.1.5](https://bitbucket.org/ridermansousa/semantic-release-sample/compare/v1.1.4...v1.1.5) (2019-09-11)


### Bug Fixes

* **release:** bing back release notes generator ([53831ec](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/53831ec))

## [1.1.3](https://bitbucket.org/ridermansousa/semantic-release-sample/compare/v1.1.2...v1.1.3) (2019-09-11)


### Bug Fixes

* **release:** remove debug config ([106cf4c](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/106cf4c))

## [1.1.2](https://bitbucket.org/ridermansousa/semantic-release-sample/compare/v1.1.1...v1.1.2) (2019-09-11)


### Bug Fixes

* **release:** add debug to release ([d94fafb](https://bitbucket.org/ridermansousa/semantic-release-sample/commits/d94fafb))
