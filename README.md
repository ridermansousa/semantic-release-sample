# semantic-release-maxmilhas-npm

> This is just sample project, used to demonstration how to semantic release works
> export default configuration for semantic release

[**semantic-release**](https://github.com/semantic-release/semantic-release) shareable config to MaxMilhas NPM Packages.

[![npm version](https://img.shields.io/npm/v/semantic-release-maxmilhas-npm?logo=npm&style=flat-square)](https://www.npmjs.com/package/semantic-release-maxmilhas-npm)

## Plugins

This shareable configuration use the following plugins:

- [`@semantic-release/commit-analyzer`](https://github.com/semantic-release/commit-analyzer)
- [`@semantic-release/release-notes-generator`](https://github.com/semantic-release/release-notes-generator)
- [`@semantic-release/changelog`](https://github.com/semantic-release/changelog)
- [`@semantic-release/git`](https://github.com/semantic-release/git)
- [`@semantic-release/npm`](https://github.com/semantic-release/npm)
- [`semantic-release-slack-bot`](https://github.com/juliuscc/semantic-release-slack-bot)

## Install

```bash
$ npm install --save-dev semantic-release-maxmilhas-npm
```

## Usage

The shareable config can be configured in the [**semantic-release** configuration file](https://github.com/semantic-release/semantic-release/blob/master/docs/usage/configuration.md#configuration):

```json
{
  "extends": "semantic-release-maxmilhas-npm"
}
```

## Configuration

See each [plugin](#plugins) documentation for required installation and configuration steps.

Other options use their default values. See each [plugin](#plugins) documentation for available options.
